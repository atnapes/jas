<?php

function strip_zeros_from_date($marked_string = "") {
    // first remove the marked zeros
    $no_zeros = str_replace('*0', '', $marked_string);
    // then remove any remaining marks
    $cleaned_string = str_replace('*', '', $no_zeros);
    return $cleaned_string;
}

function redirect_to($location = NULL) {
    if ($location != NULL) {
        header("Location: {$location}");
        exit();
    }
}

function output_message($message = "") {
    if (!empty($message)) {
        return "<p class=\"message\">{$message}</p>";
    } else {
        return "";
    }
}

function __autoload($class_name) {
    $class_name = strtolower($class_name);
    $path = LIB_PATH . DS . "{$class_name}.php";
    if (file_exists($path)) {
        require_once ($path);
    } else {
        die("The file {$class_name}.php could not be found.");
    }
}

function include_layout_template($template = "") {
    include (SITE_ROOT . DS . 'public' . DS . 'layouts' . DS . $template);
}

function log_action($action, $message = "") {
    $handle = fopen(SITE_ROOT . "/logs/log.txt", "a");
    if (!is_writable(SITE_ROOT . "/logs/log.txt"))
        die("the file is not writable");
    $date_time = date("Y-m-d H:i:s");
    $text = "[$date_time] [$action] message: $message\n";
    fwrite($handle, $text);
    fclose($handle);

    // file_put_contents(SITE_ROOT."/logs/log.txt", 'foo');
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    $data = mysql_real_escape_string($data);
    return $data;
}

function en_to_fa($text) {
    $en = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $fa = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    return str_replace($en, $fa, $text);
}

function empty_subarrays($array) {
    $empty = true;
    foreach ($array as $subarray) {
        if (!empty_array($subarray))
            $empty = false;
    }
    return $empty;
}

function empty_array($array) {
    $empty = true;
    foreach ($array as $value) {
        if ($value != "")
            $empty = false;
    }
    return $empty;
}

function nationalNumberExists($needle) {
    // Query job data from database
    $query = sprintf("SELECT nationalNumber FROM Records");
    $results = mysql_query($query);
    while ($row = mysql_fetch_assoc($results)) {
        if ($row['nationalNumber'] == $needle) {
            return true;
        }
    }
    return false;
}

function export_csv($records, $file) {
//    $file = '../public/admin/' . $file;
    unlink($file);
    echo $file;
//    echo $db;
    // fetching all the jobs
    $query = sprintf("SELECT JobID, JobName FROM Jobs");
    $result_set = mysql_query($query);
    $jobs = array();
    while ($row = mysql_fetch_assoc($result_set))
        $jobs [$row ['JobID']] = $row ['JobName'];
    
    $handle = fopen($file, "w");
    if (!is_writable($file))
        die("the file is not writable");

    $text = 'آدرس پروفایل,کد پیگیری,نام و نام خانوادگی,نام مستعار,نام پدر,کد ملی,محل تولد,وضعیت تاهل,جنسیت,وضعیت نظام وظیفه,';
    $text .= 'تعداد فرزندان,تلفن تماس,تلفن همراه,ایمیل,نشانی,تلفن محل کار,نشانی محل کار,نوع همکاری,حقوق مورد نظر,';
    $text .= 'گواهینامه رانندگی,اولویت اول,اولویت دوم,اولویت سوم,سوابق تحصیلی,سوابق کاری,دوره‌های آموزش کامپیوتر,';
    $text .= 'مهارت کامپیوتری,زبان‌های خارجی,گواهینامه‌های علمی تخصصی' . "\r\n";

    fwrite($handle, $text);

    foreach ($records as $record) {
        $studies = print_r(unserialize($record ['studies']), true);
        $works = print_r(unserialize($record ['works']), true);
        $computerCourses = print_r(unserialize($record ['computerCourses']), true);
        $computerProficiencies = print_r(unserialize($record ['computerProficiencies']), true);
        $languages = print_r(unserialize($record ['languages']), true);
        $certificates = print_r(unserialize($record ['certificates']), true);
        $profileURL = $_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF']);
        $profileURL .= DS . 'profile.php?id=' . $record ['recordID'];

        $text = $profileURL . ",386" . $record ['recordID'] . "," . $record ['fullName'] . "," . $record ['alias'] . ",";
        $text .= $record ['fatherName'] . "," . $record ['nationalNumber'] . "," . $record ['birthPlace'] . ",";
        $text .= $record ['maritalStatus'] . "," . $record ['gender'] . "," . $record ['conscriptionStatus'] . ",";
        $text .= $record ['childrenNumber'] . "," . $record ['phoneNumber'] . "," . $record ['cellPhoneNumber'] . ",";
        $text .= $record ['email'] . "," . $record ['address'] . "," . $record ['currentWorkplacePhone'] . ",";
        $text .= $record ['currentWorkplaceAddress'] . "," . $record ['intendedWorkType'] . "," . $record ['intendedSalary'] . ",";
        $text .= $record ['driverLicense'] . "," . $jobs[$record ['firstPriority']] . "," . $jobs[$record ['secondPriority']] . ",";
        $text .= $jobs[$record ['thirdPriority']] . "," . $studies . "," . $works . "," . $computerCourses . ",";
        $text .= $computerProficiencies . "," . $languages . "," . $certificates . "\r\n";

        
        $needle = "\n";
        $replace = " ";
        $text = str_replace($needle, $replace, $text);
        $preg = "/ +/i";
        $text = preg_replace($preg, $replace, $text);

        fwrite($handle, $text);
    }
    fclose($handle);
    chmod($file, 0400);
}

function getApplicatedJobs() {
    $query = sprintf("SELECT firstPriority, secondPriority, thirdPriority "
            . "FROM Records;");
    $result_set = mysql_query($query);
    $applicated_jobs = array();
    while ($row = mysql_fetch_assoc($result_set)) {
        $fp = $row['firstPriority'];
        $sp = $row['secondPriority'];
        $tp = $row['thirdPriority'];
        $applicated_jobs [] = $fp;
        if ($sp != "" && $sp != $fp)
            $applicated_jobs [] = $sp;
        if ($tp != "" && $tp != $sp && $tp != $fp)
            $applicated_jobs [] = $tp;
    }
    return array_count_values($applicated_jobs);
}

?>