(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

	jQuery.extend(jQuery.validator.messages, {
    required: "این گزینه الزامی است.",
    remote: "لطفا این گزینه را اصلاح کنید.",
    email: "لطفا یک ایمیل معتبر وارد کنید.",
    url: "لطفا یک نشانی وب معتبر وارد کنید.",
    date: "لطفا یک تاریخ معتبر وارد کنید.",
    dateISO: "لطفا یک تاریخ معتبر وارد کنید (ISO).",
    number: "لطفا یک عدد معتبر وارد کنید.",
    digits: "لطفا فقط عدد وارد کنید.",
    creditcard: "لطفا یک شماره کارت معتبر وارد کنید.",
    equalTo: "لطفا همان گزینه را دوباره وارد کنید.",
    accept: "فرمت قابل قبول نیست.",
    maxlength: jQuery.validator.format("این گزینه نباید بیشتر از {0} حرف باشد."),
    minlength: jQuery.validator.format("این گزینه نباید کمتر از {0} حرف باشد."),
    rangelength: jQuery.validator.format("این گزینه باید بین  {0} تا {1} حرف باشد."),
    range: jQuery.validator.format("عددی بین {0} و {1} وارد کنید."),
    max: jQuery.validator.format("عددی کمتر یا برابر با {0} وارد کنید."),
    min: jQuery.validator.format("عددی بیشتر یا برابر با {0} وارد کنید.")
	});
	
}));

$(function() {

	
	var form = $("#job-application-form").show();

	form.steps({
		headerTag: "h3",
		bodyTag: "fieldset",
	    autoFocus: true,
	    enableFinishButton: false,
	    showFinishButtonAlways: false,
	    enableCancelButton: false,
	    enableKeyNavigation: false,
		transitionEffect: "slide",
		onStepChanging: function (event, currentIndex, newIndex)
		{
			// Allways allow previous action even if the current form is not valid!
			if (currentIndex > newIndex)
			{
				return true;
			}
//			// Forbid next action on "Warning" step if the user is too young
//			if (newIndex === 3 && Number($("#age-2").val()) < 18)
//			{
//				return false;
//			}
			// Needed in some cases if the user went back (clean up)
			if (currentIndex < newIndex)
			{
				// To remove error styles
				form.find(".body:eq(" + newIndex + ") label.error").remove();
				form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
			}
			form.validate().settings.ignore = ":disabled,:hidden";
//			console.log(form.valid());
//			return true;
			return form.valid();
		},
		onStepChanged: function (event, currentIndex, priorIndex)
		{
//			// Used to skip the "Warning" step if the user is old enough.
//			if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
//			{
//				form.steps("next");
//			}
//			// Used to skip the "Warning" step if the user is old enough and wants to the previous step.
//			if (currentIndex === 2 && priorIndex === 3)
//			{
//				form.steps("previous");
//			}
		},
		onFinishing: function (event, currentIndex)
		{
			form.validate().settings.ignore = ":disabled";
//			form.submit();
			return form.valid();

			return true;
		},
		onFinished: function (event, currentIndex)
		{
			form.submit();
		},
		
		
		
		/* Labels */
	    labels: {
	        cancel: "لـغــو",
	        current: "گـام جــاری:",
	        pagination: "صفحه بندی",
	        finish: "ارســـال",
	        next: "بعـدی",
	        previous: "قبلـی",
	        loading: "در حال بارگیری..."
	    }
	})
	.validate({
		errorPlacement: function errorPlacement(error, element) { element.before(error); },
		rules: {
			photo: {
				required: true,
				accept: "jpeg, jpg, pjpeg",
				filesize: 71681
			}
		}
	});
	
	
	
	
	
	// persian date picker script
	$('input[data-persian-date]').pDatepicker({ format : "YYYY/MM/DD"}).val('');
	
	fillName();
	disableConscriptionInput();
	traverse($('div.steps')[0]);
	
	for (var job in jobs) {
		var jobName = jobs[job].JobName;
	}
	
	$.validator.addMethod('filesize', function(value, element, param) {
	    // param = size (in bytes) 
	    // element = element to validate (<input>)
	    // value = value of the element (file name)
	    return this.optional(element) || (element.files[0].size <= param)
	});
});

$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'فایل باید به فرمت JPG و  کمتر از ۷۰ کیلوبایت باشد.');

var studyRecords = workRecords = computerCourseRecords = 1;
var computerProficiencyRecords = foreignLangRecords = certificateRecords = 1;

function addRecord(record) {
	var recordNumber = 0;
	switch (record) {
	case "study":
		recordNumber = ++studyRecords;
		break;
	case "work":
		recordNumber = ++workRecords;
		break;
	case "computerCourse":
		recordNumber = ++computerCourseRecords;
		break;
	case "computerProficiency":
		recordNumber = ++computerProficiencyRecords;
		break;
	case "foreignLang":
		recordNumber = ++foreignLangRecords;
		break;
	case "certificate":
		recordNumber = ++certificateRecords;
		break;
	}
	var firstRow = $('div#' + record + '_1').clone().wrap('<p>').parent().html();
	var newRow = firstRow.replace(/_1/g, "_" + recordNumber);
	var newRow = newRow.replace(/data-persian-date/g, "data-persian-date-" + recordNumber);
	var hr = '<div class="row"><hr></div>';
	$('#' + record + 'RefRow').before(hr + newRow);
	
	// persian date picker scripts
	$('input[data-persian-date-' + recordNumber + ']').pDatepicker({ format : "YYYY/MM/DD"}).val('');
}

function disableConscriptionInput() {
    if (document.getElementById('female').checked)
        document.getElementById('cnscStatus').style.display = 'none';
    else 
    	document.getElementById('cnscStatus').style.display = 'block';
}

function fillName () {
	var fullName = $("#firstName").val() + " " + $("#lastName").val();
	$("#subscriberName").html(fullName);
}

function setConstraints(priorityNumber) {
	var selectBox = document.getElementById("priority_" + priorityNumber);
	if (selectBox.value == ""){
		selectBox.selectedIndex = "0";
		return;
	}
	var selectedJob;
	for (var job in jobs) {
		if (selectBox.value == jobs[job].JobID) {
			selectedJob = jobs[job];
			break;
		}
	}
	$('#pSex_' + priorityNumber).html(selectedJob.Sex);
	$('#pCertificate_' + priorityNumber).html(selectedJob.RequiredCertificate);
	if (typeof selectedJob.Experience == "string") {
		$('#pExperience_' + priorityNumber).html(selectedJob.Experience + " سال");
	} else {
		$('#pExperience_' + priorityNumber).html("--");
	}
	$('#pApplicant_' + priorityNumber).html(applicatedJobs[selectedJob.JobID] + " نفر");
}

function detectDuplicates () {
	for (var job in jobs) {
		for (var jib in jobs) {
			if ( jobs[job].JobID != jobs[jib].JobID && jobs[job].JobName == jobs[jib].JobName ) {
				console.log(jobs[job]);
				console.log(jobs[jib]);
			}
		}
	}
}

persian={0:'۰',1:'۱',2:'۲',3:'۳',4:'۴',5:'۵',6:'۶',7:'۷',8:'۸',9:'۹'};
function traverse(el){
    if(el.nodeType==3){
        var list=el.data.match(/[0-9]/g);
        if(list!=null && list.length!=0){
            for(var i=0;i<list.length;i++)
                el.data=el.data.replace(list[i],persian[list[i]]);
        }
    }
    for(var i=0;i<el.childNodes.length;i++){
        traverse(el.childNodes[i]);
    }
}


