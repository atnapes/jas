<?php require_once("../includes/initialize.php"); ?>

<?php include_layout_template('header.php'); ?>

<?php




// Max file size for updating picture
$max = 70 * 1024;

// individual infos
$firstName = $lastName = $fullName = $alias = $fatherName = $nationalNumber = $birthPlace = "";
$birthDate = $maritalStatus = $gender = $conscriptionStatus = "";
$childrenNumber = $phoneNumber = $cellPhoneNumber = $email = $address = "";
$currentWorkplacePhone = $currentWorkplaceAddress = "";
$intendedWorkType = $intendedSalary = "";
$driverLicense = array ();

// studies
$std_grade_1 = $std_degree_1 = $std_orientation_1 = $std_from_1 = "";
$std_to_1 = $std_school_1 = $std_educationPlace_1 = $std_rate_1 = "";

// works
$wrk_companyName_1 = $wrk_work_from_1 = $wrk_work_to_1 = $wrk_post_1 = "";
$wrk_salary_1 = $wrk_endCause_1 = $wrk_experienceCertificate_1 = $wrk_workType_1 = "";

// computer courses
$ccs_courseName_1 = $ccs_level_1 = $ccs_courseSchool_1 = $ccs_year_1 = "";
$ccs_courseClassDuration_1 = $ccs_cCourseCertificate_1 = "";

// computer programs
$cpr_applicationName_1 = $cpr_applicationProficiency_1 = "";

// languages
$lng_language_1 = $lng_proficiencyType_1 [] = $lng_proficiency_1 = "";

// certificates
$crt_certificateType_1 = $crt_certificateSchool_1 = $crt_earnYear_1 = "";
$crt_certCourseHoursLenght_1 = $crt_courseCertificate_1 = "";

// priorities
$firstPriority = $secondPriority = $thirdPriority = "";

if ($_SERVER ["REQUEST_METHOD"] == "POST") {

	$firstName = test_input ( $_POST ["firstName"] );
	$lastName = test_input ( $_POST ["lastName"] );
	$fullName = $firstName . " " . $lastName;
	$alias = test_input ( $_POST ["alias"] );
	$fatherName = test_input ( $_POST ["fatherName"] );
	$nationalNumber = test_input ( $_POST ["nationalNumber"] );
	$birthPlace = test_input ( $_POST ["birthPlace"] );
	$birthDate = test_input ( $_POST ["birthDate"] );
	$maritalStatus = test_input ( $_POST ["maritalStatus"] );
	$gender = test_input ( $_POST ["gender"] );
	$conscriptionStatus = ($gender == 'زن') ?
                '--' : test_input ( $_POST ["conscriptionStatus"] );
	$childrenNumber = test_input ( $_POST ["childrenNumber"] );
	$phoneNumber = test_input ( $_POST ["phoneNumber"] );
	$cellPhoneNumber = test_input ( $_POST ["cellPhoneNumber"] );
	$email = test_input ( $_POST ["email"] );
	$address = test_input ( $_POST ["address"] );
	$currentWorkplacePhone = test_input ( $_POST ["currentWorkplacePhone"] );
	$currentWorkplaceAddress = test_input ( $_POST ["currentWorkplaceAddress"] );
	$intendedWorkType = test_input ( $_POST ["intendedWorkType"] );
	$intendedSalary = test_input ( $_POST ["intendedSalary"] );
	$driverLicense = serialize ( $_POST ["driverLicense"] );
	
	$studies = array ();
	foreach ( $_POST as $key => $value ) {
		static $std_counter = 0;
		if (strpos ( $key, 'std_' ) === 0) {
			if (strpos ( $key, 'std_grade' ) === 0)
				$std_counter ++;
			$prefix = 'std_';
			$sufix = '_' . $std_counter;
			$replace = '';
			$key = str_replace ( $prefix, $replace, $key );
			$key = str_replace ( $sufix, $replace, $key );
			$studies ["study_" . $std_counter] [$key] = $value;
		}
	}
	$studies = serialize ( $studies );
	
	$works = array ();
	foreach ( $_POST as $key => $value ) {
		static $wrk_counter = 0;
		if (strpos ( $key, 'wrk_' ) === 0) {
			if (strpos ( $key, 'wrk_companyName' ) === 0)
				$wrk_counter ++;
			$prefix = 'wrk_';
			$sufix = '_' . $wrk_counter;
			$replace = '';
			$key = str_replace ( $prefix, $replace, $key );
			$key = str_replace ( $sufix, $replace, $key );
			$works ["work_" . $wrk_counter] [$key] = $value;
		}
	}
	$works = serialize ( $works );
	
	$computerCourses = array ();
	foreach ( $_POST as $key => $value ) {
		static $ccs_counter = 0;
		if (strpos ( $key, 'ccs_' ) === 0) {
			if (strpos ( $key, 'ccs_courseName' ) === 0)
				$ccs_counter ++;
			$prefix = 'ccs_';
			$sufix = '_' . $ccs_counter;
			$replace = '';
			$key = str_replace ( $prefix, $replace, $key );
			$key = str_replace ( $sufix, $replace, $key );
			$computerCourses ["computer_course_" . $ccs_counter] [$key] = $value;
		}
	}
	$computerCourses = serialize ( $computerCourses );
	
	$computerProficiencies = array ();
	foreach ( $_POST as $key => $value ) {
		static $cpr_counter = 0;
		if (strpos ( $key, 'cpr_' ) === 0) {
			if (strpos ( $key, 'cpr_applicationName' ) === 0)
				$cpr_counter ++;
			$prefix = 'cpr_';
			$sufix = '_' . $cpr_counter;
			$replace = '';
			$key = str_replace ( $prefix, $replace, $key );
			$key = str_replace ( $sufix, $replace, $key );
			$computerProficiencies ["computer_proficiency_" . $cpr_counter] [$key] = $value;
		}
	}
	$computerProficiencies = serialize ( $computerProficiencies );
	
	$languages = array ();
	foreach ( $_POST as $key => $value ) {
		static $lng_counter = 0;
		if (strpos ( $key, 'lng_' ) === 0) {
			if (strpos ( $key, 'lng_language' ) === 0)
				$lng_counter ++;
			$prefix = 'lng_';
			$sufix = '_' . $lng_counter;
			$replace = '';
			$key = str_replace ( $prefix, $replace, $key );
			$key = str_replace ( $sufix, $replace, $key );
			$languages ["language_" . $lng_counter] [$key] = $value;
		}
	}
	$languages = serialize ( $languages );
	
	$certifications = array ();
	foreach ( $_POST as $key => $value ) {
		static $crt_counter = 0;
		if (strpos ( $key, 'crt_' ) === 0) {
			if (strpos ( $key, 'lng_language' ) === 0)
				$crt_counter ++;
			$prefix = 'crt_';
			$sufix = '_' . $crt_counter;
			$replace = '';
			$key = str_replace ( $prefix, $replace, $key );
			$key = str_replace ( $sufix, $replace, $key );
			$certifications ["certificate_" . $crt_counter] [$key] = $value;
		}
	}
	$certifications = serialize ( $certifications );
	
	$firstPriority = test_input ( $_POST ["firstPriority"] );
	$secondPriority = test_input ( $_POST ["secondPriority"] );
	$thirdPriority = test_input ( $_POST ["thirdPriority"] );

	
	// if first priority has been selected store the form in the database
	if (nationalNumberExists($_POST['nationalNumber'])) {
		$post = serialize($_POST);
		log_action('national number exists in the table', $post);
		echo "<script>alert('شما قبلا در سامانه ثبت‌نام کرده‌اید.')</script>";
	}
	else if ($_POST ["firstPriority"] == 0) {
		$post = serialize($_POST);
		log_action('first priority not set', $post);
		echo "<script>alert('مشکلی رخ داد. لطفا دوباره امتحان کنید.')</script>";
	} else {
		$sql  = "INSERT INTO Records " . "(`fullName`, `alias`, `fatherName`, `nationalNumber`, `birthPlace`, ";
		$sql .= "`birthDate`, `maritalStatus`, `gender`, `conscriptionStatus`, `childrenNumber`, ";
		$sql .= "`phoneNumber`, `cellPhoneNumber`, `email`, `address` ,`currentWorkplacePhone`, ";
		$sql .= "`currentWorkplaceAddress`, `intendedWorkType`, `intendedSalary`, `driverLicense`, ";
		$sql .= "`studies`, `works`, `computerCourses`, `computerProficiencies`, `languages`, ";
		$sql .= "`certifications`, `firstPriority`, `secondPriority`, `thirdPriority`) ";
		$sql .= "VALUES ";
		$sql .= "('$fullName', '$alias', '$fatherName', '$nationalNumber', '$birthPlace', ";
		$sql .= "'$birthDate', '$maritalStatus', '$gender', '$conscriptionStatus', '$childrenNumber', ";
		$sql .= "'$phoneNumber', '$cellPhoneNumber', '$email', '$address' ,'$currentWorkplacePhone', ";
		$sql .= "'$currentWorkplaceAddress', '$intendedWorkType', '$intendedSalary', '$driverLicense', ";
		$sql .= "'$studies', '$works', '$computerCourses', '$computerProficiencies', '$languages', ";
		$sql .= "'$certifications', '$firstPriority', '$secondPriority', '$thirdPriority')";
		
                $test = false;
                if ($test) {
                    log_action($sql);
                    $success = true;
                } else {
                    $success = $database->query ( $sql );
                }
		// If insertion was successful
		if ($success) {
			// Uploading picture
			$upload_result = array ();
			$destination = __DIR__ . '/_assets/';
			try {
				// I modified the UpdateFile class to change the file name at upload
				$upload = new UploadFile ( $destination );
				$upload->setMaxSize ( $max );
				$upload->upload ( $database->insert_id (), false );
				$upload_result = $upload->getMessages ();
			} catch ( Exception $e ) {
				$upload_result [] = $e->getMessage ();
			}
			$error = error_get_last();
			log_action ( "file upload message", "Upload results: " . print_r($upload_result, true));
			
			$tracking_code = '386' . $database->insert_id ();
			
 			// Send registration info to the management via email
			$to = 'revok.ir@gmail.com';
			$cc = 'atnapes@gmail.com';
			$subject = 'ثبت‌نام جدید: ' . $database->insert_id();
			$message = print_r($_POST, true);
			$headers = 'From: sabtenam@kticket.ir' . "\r\n";
			mail($to, $subject, $message, $headers);
// 			mail($cc, $subject, $message, $headers);
			
			$success_message = "اطلاعات شما با کد پیگیری $tracking_code ثبت شد در صورت تائيد اوليه و قرارگرفتن در ليست افرادى كه شرايط مورد نظر را دارند ،جهت مصاحبه حضورى با شما تماس گرفته خواهد شد.";
			
			// Sending the success message to registrar via sms if the entered phone number seems correct
			if (strlen ( $cellPhoneNumber ) > 10 && strlen ( $cellPhoneNumber ) < 14) {
				require_once ("../includes/send_sms.php");
				kticket_send_sms ( $cellPhoneNumber, $success_message );
				log_action ( "sms", "sent sms request for $cellPhoneNumber phone number" );
			}
			// prevent resubmission
			redirect_to("submit.php?success=$success&tracking_code=$tracking_code");
		} else {
			// Send insertion failure data to the developer via sms
			require_once ("../includes/send_sms.php");
			kticket_send_sms ( '09307863940', 'mysql_error occured in jas' );
		}
	}
}

// Query job data from database
$query = sprintf ( "SELECT JobID, JobName, Sex, RequiredCertificate,
		Experience FROM `Jobs` WHERE Open=1 group by JobName order by JobID" );
$result_set = $database->query ( $query );
if (! $result_set) {
	$message = 'Invalid query: ' . mysql_error () . "\n";
	$message .= 'Whole query: ' . $query;
	log_action ( 'error', $message );
	die ();
}
?>

<form id="job-application-form"
	action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"
	method="post" enctype="multipart/form-data">




	<!-- گام اول -->
	<h3>مشخصات</h3>
	<fieldset>

		<!-- first step title -->
		<legend>مشخصات فردی</legend>

		<div class="row">
			<div class="col-sm-12">
				<ul id="errorBoard">
					<li>فرم را به فارسی پر کنید.</li>
					<li>گزینه‌های ضروری با <span class="error">*</span> مشخص شده‌اند.
					</li>
					<?php
					// if ($error)
					// echo "<li>{$error['message']}</li>";
					// if ($result)
					// foreach ($result as $message)
					// echo "<li>$message</li>";
					?>
				</ul>
			</div>

			<br>
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-6">
				<label for="firstName">نام: <span class="error">*</span></label>
				<input id="firstName" name="firstName" type="text" maxlength="20"
					minlength="2" onchange="fillName()" value="<?php echo $firstName;?>"
					autofocus required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="lastName">نام خانوادگی: <span class="error">*</span></label>
				<input id="lastName" name="lastName" type="text" maxlength="20"
					minlength="2" onchange="fillName()" value="<?php echo $lastName;?>"
					autofocus required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="alias">نام مستعار:</label> <input id="alias"
					maxlength="10" minlength="2" name="alias" type="text"
					value="<?php echo $alias;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="fatherName">نام پدر:<span class="error">*</span></label>
				<input id="fatherName" name="fatherName" type="text" maxlength="10"
					minlength="2" value="<?php echo $fatherName;?>" required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="nationalNumber">شماره ملی: <span class="error">*</span></label>
				<input id="nationalNumber" name="nationalNumber" type="number"
					maxlength="10" minlength="10" value="<?php echo $nationalNumber;?>"
					pattern="[0-9]{10}" required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="birthPlace">محل تولد: <span class="error">*</span></label>
				<input id="birthPlace" name="birthPlace" type="text" maxlength="15"
					minlength="2" value="<?php echo $birthPlace;?>" required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="birthDate">تاریخ تولد: <span class="error">*</span></label>
				<input id="birthDate" name="birthDate" type="text" data-persian-date
					value="<?php echo $birthDate;?>" placeholder="1395/05/13" required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="maritalStatus">وضعیت تاهل:<span class="error">*</span></label>
				<table>
					<tr>
						<td><input name="maritalStatus" type="radio" value="مجرد"
							id="single" required
							<?php if ((isset($maritalStatus)) && ($maritalStatus==='مجرد')) echo "checked"; ?>>
							<label for="single">مجرد</label></td>
						<td><input name="maritalStatus" type="radio" value="متاهل"
							id="married" required
							<?php if ((isset($maritalStatus)) && ($maritalStatus==='متاهل')) echo "checked"; ?>>
							<label for="married">متاهل</label></td>
					</tr>
				</table>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="gender">جنسیت:<span class="error">*</span></label>
				<table>
					<tr>
						<td><input name="gender" type="radio" value="مرد" id="male"
							required onclick="disableConscriptionInput()"
							<?php if ((isset($gender)) && ($gender==='مرد')) echo "checked"; ?>>
							<label for="male">مرد</label></td>
						<td><input name="gender" type="radio" value="زن" id="female"
							required onclick="disableConscriptionInput()"
							<?php if ((isset($gender)) && ($gender==='زن')) echo "checked"; ?>>
							<label for="female">زن</label></td>
					</tr>
				</table>
			</div>
			<div class="col-md-4 col-sm-6" id="cnscStatus">
				<label for="conscriptionStatus">وضعیت نظام وظیفه:<span class="error">*</span></label>
				<input type="hidden" name="conscriptionStatus" value="خانم">
				<table>
					<tr>
						<td><input name="conscriptionStatus" type="radio"
							value="پایان خدمت" id="finished" required
							<?php if ((isset($conscriptionStatus)) && ($conscriptionStatus==='پایان خدمت')) echo "checked"; ?>>
							<label for="finished">پایان خدمت</label></td>
						<td><input name="conscriptionStatus" type="radio"
							value="معاف دائم" id="permanentExemption" required
							<?php if ((isset($conscriptionStatus)) && ($conscriptionStatus==='معاف دائم')) echo "checked"; ?>>
							<label for="permanentExemption">معاف دائم</label></td>
						<td><input name="conscriptionStatus" type="radio"
							value="معاف موقت" id="temporaryExemption" required
							<?php if ((isset($conscriptionStatus)) && ($conscriptionStatus==='معاف موقت')) echo "checked"; ?>>
							<label for="temporaryExemption">معاف موقت</label></td>
					</tr>
				</table>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="childrenNumber">تعداد فرزندان:<span class="error">*</span></label>
				<input id="childrenNumber" name="childrenNumber" type="number"
					min="0" max="20" value="<?php echo $childrenNumber;?>" required>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="phoneNumber">تلفن تماس:<span class="error">*</span></label>
				<input id="phoneNumber" name="phoneNumber" type="number" required
					maxlength="15" minlength="5" placeholder="۰۳۱۵۵۵۵۵۵۵۵"
					value="<?php echo $phoneNumber;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="cellPhoneNumber">تلفن همراه:<span class="error">*</span></label>
				<input id="cellPhoneNumber" name="cellPhoneNumber" required
					maxlength="15" minlength="5" type="number"
					value="<?php echo $cellPhoneNumber;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="email">ایمیل:</label> <input id="email" name="email"
					type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
					maxlength="30" value="<?php echo $email;?>">
			</div>
			<div class="col-md-8 col-sm-6">
				<label for="address">نشانی محل سکونت:<span class="error">*</span></label>
				<input id="address" name="address" type="text" style="width: 82%"
					required maxlength="250" minlength="10"
					value="<?php echo $address;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="currentWorkplacePhone">تلفن محل کار فعلی:</label> <input
					id="currentWorkplacePhone" name="currentWorkplacePhone" type="text"
					maxlength="15" minlength="5"
					value="<?php echo $currentWorkplacePhone;?>"
					placeholder="۰۳۱۵۵۵۵۵۵۵۵">
			</div>
			<div class="col-md-8 col-sm-6">
				<label for="currentWorkplaceAddress">نشانی محل کار فعلی:</label> <input
					id="currentWorkplaceAddress" name="currentWorkplaceAddress"
					type="text" style="width: 82%" maxlength="250" minlength="10"
					value="<?php echo $currentWorkplaceAddress;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="desiredWorkType">به چه صورت تمایل به همکاری دارید؟<span
					class="error">*</span></label>
				<table>
					<tr>
						<td><input name="intendedWorkType" type="radio" value="تمام‌وقت"
							id="dfullTime_1" required
							<?php if ((isset($intendedWorkType)) && ($intendedWorkType==='تمام‌وقت')) echo "checked"; ?>>
							<label for="dfullTime_1">تمام‌وقت</label></td>
						<td><input name="intendedWorkType" type="radio" value="پاره‌وقت"
							id="dpartTime_1" required
							<?php if ((isset($intendedWorkType)) && ($intendedWorkType==='پاره‌وقت')) echo "checked"; ?>>
							<label for="dpartTime_1">پاره‌وقت</label></td>
						<td><input name="intendedWorkType" type="radio" value="مشاوره"
							id="dconsultation_1" required
							<?php if ((isset($intendedWorkType)) && ($intendedWorkType==='مشاوره')) echo "checked"; ?>>
							<label for="dconsultation_1">مشاوره</label></td>
					</tr>
				</table>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="intendedSalary">حقوق مورد نظر(ریال):</label> <input
					id="intendedSalary" name="intendedSalary" maxlength="8"
					minlength="6" type="number" value="<?php echo $intendedSalary;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="driverLicense">گواهینامه رانندگی:</label>
				<table>
					<tr>
						<td><input name="driverLicense[]" type="checkbox"
							value="موتورسیکلت" id="motorcycle"
							<?php if ((isset($driverLicense)) && (in_array('موتورسیکلت', $driverLicense))) echo "checked"; ?>>
							<label for="motorcycle">موتورسیکلت</label></td>
					</tr>
					<tr>
						<td><input name="driverLicense[]" type="checkbox"
							value="خودرو پایه یک" id="firstGrade"
							<?php if ((isset($driverLicense)) && (in_array('موتورسیکلت', $driverLicense))) echo "checked"; ?>>
							<label for="firstGrade">خودرو پایه یک</label></td>
					</tr>
					<tr>
						<td><input name="driverLicense[]" type="checkbox"
							value="خودرو پایه دو" id="secondGrade"
							<?php if ((isset($driverLicense)) && (in_array('موتورسیکلت', $driverLicense))) echo "checked"; ?>>
							<label for="secondGrade">خودرو پایه دو</label></td>
					</tr>
				</table>
			</div>
		</div>
		<br> <br> <br>
		<legend>بارگذاری عکس</legend>

		<div class="col-md-8 col-sm-12">
			<ul>
				<li><strong>تصویر داوطلب لازم است تمام رخ بوده و در سال جاری گرفته
						شده باشد</strong></li>
				<li>فایل تصویر متقاضی لازم است از تصویر ۳×۳ یا ۶×۴ تهیه شده و با
					فرمت JPG ذخیره شده باشد.</li>
				<li>اندازه عكس اسكن شده بايد حداكثر ۴۰۰×۳۰۰ پيكسل و حداقل ۳۰۰×۲۰۰
					باشد.</li>
				<li>حجم فايل ذخيره شده عكس نبايد از ۷۰ كيلو بايت بيشتر باشد.</li>
			</ul>
			<label for="photo">بارگذاری عکس:</label> <input type="hidden"
				name="MAX_FILE_SIZE" value="<?php echo $max;?>"> <input id="photo"
				name="photo" type="file">
		</div>
	</fieldset>




	<!-- گام دوم -->
	<h3>سوابق</h3>
	<fieldset>
		<legend>سوابق تحصیلی</legend>
		<div id="study_1" class="row">
			<div class="col-md-4 col-sm-6">
				<label for="std_grade_1">مقطع:</label> <select id="grade_1"
					name="std_grade_1">
					<option value="">انتخاب کنید...</option>
					<option value="دیپلم" data-foo="this is the sound of dj alligator"
						<?php if ((isset($std_grade_1)) && ($std_grade_1 === 'دیپلم')) echo "selected";?>>دیپلم</option>
					<option value="فوق دیپلم" data-foo="this is the sound of dj ramin"
						<?php if ((isset($std_grade_1)) && ($std_grade_1 === 'فوق دیپلم')) echo "selected";?>>فوق
						دیپلم</option>
					<option value="لیسانس"
						<?php if ((isset($std_grade_1)) && ($std_grade_1 === 'لیسانس')) echo "selected";?>>لیسانس</option>
					<option value="فوق لیسانس"
						<?php if ((isset($std_grade_1)) && ($std_grade_1 === 'فوق لیسانس')) echo "selected";?>>فوق
						لیسانس</option>
				</select>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_degree_1">رشته تحصیلی:</label> <input
					id="std_degree_1" name="std_degree_1" type="text"
					value="<?php echo $std_degree_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_orientation_1">گرایش:</label> <input
					id="std_orientation_1" name="std_orientation_1" type="text"
					value="<?php echo $std_orientation_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_from_1">مدت تحصیل از تاریخ:</label> <input
					id="std_from_1" name="std_from_1" type="text" data-persian-date
					placeholder="1395/05/13" value="<?php echo $std_from_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_to_1">تا تاریخ:</label> <input id="std_to_1"
					name="std_to_1" type="text" data-persian-date
					placeholder="1395/05/13" value="<?php echo $std_to_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_school_1">نام واحد آموزشی:</label> <input
					id="std_school_1" name="std_school_1" type="text"
					value="<?php echo $std_school_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_educationPlace_1">کشور/شهر محل تحصیل:</label> <input
					id="std_educationPlace_1" name="std_educationPlace_1" type="text"
					value="<?php echo $std_educationPlace_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="std_rate_1">معدل:</label> <input id="std_rate_1"
					name="std_rate_1" type="number" value="<?php echo $std_rate_1;?>">
			</div>
		</div>
		<div id="studyRefRow" class="row">
			<div class="col-md-4 col-sm-6">
				<a class="btn btn-default" role="button" style="margin-bottom: 1em"
					onclick="addRecord('study')">اضافه کردن سابقه تحصیلی</a>
			</div>
		</div>
		<br> <br>
		<legend>سوابق حرفه ای</legend>

		<div id="work_1" class="row">
			<div class="col-md-4 col-sm-6">
				<label for="wrk_companyName_1">نام سازمان:</label> <input
					id="wrk_companyName_1" name="wrk_companyName_1" type="text"
					value="<?php echo $wrk_companyName_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_work_from_1">مدت همکاری از تاریخ:</label> <input
					id="wrk_work_from_1" name="wrk_work_from_1" type="text"
					data-persian-date value="<?php echo $wrk_work_from_1;?>"
					placeholder="1395/05/13">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_work_to_1">تا تاریخ:</label> <input
					id="wrk_work_to_1" name="wrk_work_to_1" type="text"
					data-persian-date value="<?php echo $wrk_work_to_1;?>"
					placeholder="1395/05/13">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_post_1">سمت:</label> <input id="wrk_post_1"
					name="wrk_post_1" type="text" value="<?php echo $wrk_post_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_salary_1">حقوق و مزایا(ریال):</label> <input
					id="wrk_salary_1" name="wrk_salary_1" type="number"
					value="<?php echo $wrk_salary_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_endCause_1">علت قطع رابطه:</label> <input
					id="wrk_endCause_1" name="wrk_endCause_1" type="text"
					value="<?php echo $wrk_endCause_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_experienceCertificate_1">گواهی سابقه کار:</label>
				<table>
					<tr>
						<td><input name="wrk_experienceCertificate_1" type="radio"
							value="دارم" id="have_1"
							<?php if ((isset($wrk_experienceCertificate_1)) && ($wrk_experienceCertificate_1==='دارم')) echo "checked"; ?>>
							<label for="have_1">دارم</label></td>
						<td><input name="wrk_experienceCertificate_1" type="radio"
							value="ندارم" id="dontHave_1"
							<?php if ((isset($wrk_experienceCertificate_1)) && ($wrk_experienceCertificate_1==='ندارم')) echo "checked"; ?>>
							<label for="dontHave_1">ندارم</label></td>
					</tr>
				</table>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="wrk_workType_1">نوع کار:</label>
				<table>
					<tr>
						<td><input name="wrk_workType_1" type="radio" value="تمام‌وقت"
							id="fullTime_1"
							<?php if ((isset($wrk_workType_1)) && ($wrk_workType_1==='تمام‌وقت')) echo "checked"; ?>>
							<label for="fullTime_1">تمام‌وقت</label></td>
						<td><input name="wrk_workType_1" type="radio" value="پاره‌وقت"
							id="partTime_1"
							<?php if ((isset($wrk_workType_1)) && ($wrk_workType_1==='پاره‌وقت')) echo "checked"; ?>>
							<label for="partTime_1">پاره‌وقت</label></td>
						<td><input name="wrk_workType_1" type="radio" value="مشاوره"
							id="consultation_1"
							<?php if ((isset($wrk_workType_1)) && ($wrk_workType_1==='مشاوره')) echo "checked"; ?>>
							<label for="consultation_1">مشاوره</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="workRefRow" class="row">
			<div class="col-md-4 col-sm-6">
				<a class="btn btn-default" role="button" style="margin-bottom: 1em"
					onclick="addRecord('work')">اضافه کردن سابقه کاری</a>
			</div>
		</div>
	</fieldset>




	<!-- گام سوم -->
	<h3>مهارت‌ها</h3>
	<fieldset>
		<legend>دوره های آموزش کامپیوتر</legend>
		<datalist id="levels">
			<option value="مقدماتی">
			
			
			<option value="متوسط">
			
			
			<option value="پیشرفته">
		
		</datalist>
		<div id="computerCourse_1" class="row">
			<div class="col-md-4 col-sm-6">
				<label for="ccs_courseName_1">نام دوره:</label> <input
					id="ccs_courseName_1" name="ccs_courseName_1" type="text"
					value="<?php echo $ccs_courseName_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="ccs_level_1">سطح:</label> <input id="ccs_level_1"
					list="levels" name="ccs_level_1" type="text"
					value="<?php echo $ccs_level_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="ccs_courseSchool_1">محل آموزش:</label> <input
					id="ccs_courseSchool_1" name="ccs_courseSchool_1" type="text"
					value="<?php echo $ccs_courseSchool_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="ccs_year_1">سال:</label> <input id="ccs_year_1"
					name="ccs_year_1" type="number" value="<?php echo $ccs_year_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="ccs_courseClassDuration_1">میزان ساعات دوره:</label> <input
					id="ccs_courseClassDuration_1" name="ccs_courseClassDuration_1"
					type="number" value="<?php echo $ccs_courseClassDuration_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="ccs_CourseCertificate_1">گواهی پایان دوره:</label>
				<table>
					<tr>
						<td><input name="ccs_courseCertificate_1" type="radio"
							value="دارم" id="cHaveCert_1"
							<?php if ((isset($ccs_courseCertificate_1)) && ($ccs_courseCertificate_1==='دارم')) echo "checked"; ?>>
							<label for="cHaveCert_1">دارم</label></td>
						<td><input name="ccs_courseCertificate_1" type="radio"
							value="ندارم" id="cDontHaveCert_1"
							<?php if ((isset($ccs_courseCertificate_1)) && ($ccs_courseCertificate_1==='ندارم')) echo "checked"; ?>>
							<label for="cDontHaveCert_1">ندارم</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="computerCourseRefRow" class="row">
			<div class="col-md-4 col-sm-6">
				<a class="btn btn-default" role="button" style="margin-bottom: 1em"
					onclick="addRecord('computerCourse')">اضافه کردن دوره</a>
			</div>
		</div>

		<legend>مهارت کامپیوتری</legend>
		<div id="computerProficiency_1" class="row">
			<div class="col-md-4 col-sm-6">
				<label for="cpr_applicationName_1">نام برنامه:</label> <input
					id="cpr_applicationName_1" name="cpr_applicationName_1" type="text"
					value="<?php echo $cpr_applicationName_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="cpr_applicationProficiency_1">میزان تسلط:</label>
				<table>
					<tr>
						<td><input name="cpr_applicationProficiency_1" type="radio"
							value="مقدماتی" id="appElementary_1"
							<?php if ((isset($cpr_applicationProficiency_1)) && ($cpr_applicationProficiency_1==='مقدماتی')) echo "checked"; ?>>
							<label for="appElementary_1">مقدماتی</label></td>
						<td><input name="cpr_applicationProficiency_1" type="radio"
							value="متوسط" id="appIntermediate_1"
							<?php if ((isset($cpr_applicationProficiency_1)) && ($cpr_applicationProficiency_1==='متوسط')) echo "checked"; ?>>
							<label for="appIntermediate_1">متوسط</label></td>
						<td><input name="cpr_applicationProficiency_1" type="radio"
							value="پیشرفته" id="appAdvanced_1"
							<?php if ((isset($cpr_applicationProficiency_1)) && ($cpr_applicationProficiency_1==='پیشرفته')) echo "checked"; ?>>
							<label for="appAdvanced_1">پیشرفته</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="computerProficiencyRefRow" class="row">
			<div class="col-md-4 col-sm-6">
				<a class="btn btn-default" role="button" style="margin-bottom: 1em"
					onclick="addRecord('computerProficiency')">اضافه کردن مهارت
					کامپیوتری</a>
			</div>
		</div>

		<legend>مهارت در زبان خارجی</legend>
		<div id="foreignLang_1" class="row">
			<div class="col-md-4 col-sm-6">
				<label for="lng_language_1">زبان خارجی:</label> <input
					id="lng_language_1" name="lng_language_1" type="text"
					value="<?php echo $lng_language_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="lng_proficiencyType_1[]">نوع توانایی:</label>
				<table>
					<tr>
						<td><input name="lng_proficiencyType_1[]" type="checkbox"
							value="خواندن" id="reading_1"
							<?php if ((isset($lng_proficiencyType_1)) && (in_array('خواندن', $lng_proficiencyType_1))) echo "checked"; ?>>
							<label for="reading_1">خواندن</label></td>
						<td><input name="lng_proficiencyType_1[]" type="checkbox"
							value="نوشتن" id="writing_1"
							<?php if ((isset($lng_proficiencyType_1)) && (in_array('نوشتن', $lng_proficiencyType_1))) echo "checked"; ?>>
							<label for="writing_1">نوشتن</label></td>
					</tr>
					<tr>
						<td><input name="lng_proficiencyType_1[]" type="checkbox"
							value="مکالمه" id="speaking_1"
							<?php if ((isset($lng_proficiencyType_1)) && (in_array('مکالمه', $lng_proficiencyType_1))) echo "checked"; ?>>
							<label for="speaking_1">مکالمه</label></td>
						<td><input name="lng_proficiencyType_1[]" type="checkbox"
							value="شنیداری" id="listening_1"
							<?php if ((isset($lng_proficiencyType_1)) && (in_array('شنیداری', $lng_proficiencyType_1))) echo "checked"; ?>>
							<label for="listening_1">شنیداری</label></td>
					</tr>
				</table>
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="lng_proficiency_1">میزان تسلط:</label>
				<table>
					<tr>
						<td><input name="lng_proficiency_1" type="radio" value="مقدماتی"
							id="elementary_1"
							<?php if ((isset($lng_proficiency_1)) && ($lng_proficiency_1==='مقدماتی')) echo "checked"; ?>>
							<label for="elementary_1">مقدماتی</label></td>
						<td><input name="lng_proficiency_1" type="radio" value="متوسط"
							id="intermediate_1"
							<?php if ((isset($lng_proficiency_1)) && ($lng_proficiency_1==='متوسط')) echo "checked"; ?>>
							<label for="intermediate_1">متوسط</label></td>
						<td><input name="lng_proficiency_1" type="radio" value="پیشرفته"
							id="advanced_1"
							<?php if ((isset($lng_proficiency_1)) && ($lng_proficiency_1==='پیشرفته')) echo "checked"; ?>>
							<label for="advanced_1">پیشرفته</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="foreignLangRefRow" class="row">
			<div class="col-md-4 col-sm-6">
				<a class="btn btn-default" role="button" style="margin-bottom: 1em"
					onclick="addRecord('foreignLang')">اضافه کردن زبان خارجی</a>
			</div>
		</div>

		<legend>گواهینامه‌های علمی تخصصی اخذ شده</legend>
		<div id="certificate_1" class="row">
			<div class="col-md-4 col-sm-6">
				<label for="crt_certificateType_1">نوع مدرک:</label> <input
					id="crt_certificateType_1" name="crt_certificateType_1" type="text"
					value="<?php echo $crt_certificateType_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="crt_certificateSchool_1">محل اخذ:</label> <input
					id="crt_certificateSchool_1" name="crt_certificateSchool_1"
					type="text" value="<?php echo $crt_certificateSchool_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="crt_earnYear_1">سال اخذ:</label> <input
					id="crt_earnYear_1" name="crt_earnYear_1" type="number"
					value="<?php echo $crt_earnYear_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="crt_certCourseHoursLenght_1">میزان ساعت دوره:</label> <input
					id="crt_certCourseHoursLenght_1" name="crt_certCourseHoursLenght_1"
					type="text" value="<?php echo $crt_certCourseHoursLenght_1;?>">
			</div>
			<div class="col-md-4 col-sm-6">
				<label for="crt_courseCertificate_1">گواهی پایان دوره:</label>
				<table>
					<tr>
						<td><input name="crt_courseCertificate_1" type="radio"
							value="دارم" id="haveCert_1"
							<?php if ((isset($crt_courseCertificate_1)) && ($crt_courseCertificate_1==='دارم')) echo "checked"; ?>>
							<label for="haveCert_1">دارم</label></td>
						<td><input name="crt_courseCertificate_1" type="radio"
							value="ندارم" id="dontHaveCert_1"
							<?php if ((isset($crt_courseCertificate_1)) && ($crt_courseCertificate_1==='ندارم')) echo "checked"; ?>>
							<label for="dontHaveCert_1">ندارم</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="certificateRefRow" class="row">
			<div class="col-md-4 col-sm-6">
				<a class="btn btn-default" role="button" style="margin-bottom: 1em"
					onclick="addRecord('certificate')">اضافه کردن گواهینامه</a>
			</div>
		</div>
	</fieldset>




	<!-- گام چهارم -->
	<?php
	$rows = array ();
	while ( $row = mysql_fetch_assoc ( $result_set ) )
		$rows [] = $row;
	
	$job_options = "<option value=''>انتخاب کنید...</option>";
	$job_options .= "<option value='' style='color:#999' >------------- مشاغل بخش فود کورت -------------</option>";
        $temp_id = "";
	foreach ( $rows as $key => $row ) {
		$job_id = $row ['JobID'];
		$job_name = $row ['JobName'];
		if (intval($temp_id) <= 178 && intval($job_id) >= 180)
			$job_options .= "<option value='' style='color:#999' >------------- مشاغل بخش هایپرمارکت -------------</option>";
                $job_options .= '<option value="' . $job_id . '">' . $job_name . '</option>';
		$temp_id = $job_id;
	}
	?>
	<script>
		var jobs = <?php echo json_encode($rows);?>;
	</script>
	<h3>انتخاب شغل</h3>
	<fieldset>
		<div class="row">
			<div class="col-sm-12">
				<ul>
					<li>ثبت نام و انتخاب مشاغل در این سامانه هیچ حقی را برای متقاضی و
						هیچ تعهدی را مبنی بر استخدام برای شرکت ایجاد نمی کند و هر داوطلب
						باید با توجه به شرایط تخصصی و علمی خود و پس از مطالعه و تایید
						شرایط اختصاصی، شغل را به عنوان اولویت انتخابی خود ثبت نماید.</li>
					<li>هر متقاضی می‌تواند از بین مشاغل زیر بر حسب توانایی‌های خود ۳
						شغل را به ترتیب اولویت انتخاب کند.</li>
					<li>بدیهی است چنانچه متقاضی هریک از شرایط اختصاصی را نداشته باشد یا
						نتواند مدارک خواسته شده را ارائه نماید از مراحل گزینش حذف خواهد
						شد.</li>
				</ul>
			</div>

			<br>
		</div>
		<legend>اولویت اول</legend>
		<div class="row">
			<div class="col-md-12">
				<label for="firstPriority">شغل:<span style="color: red">*</span></label>
				<select id="priority_1" required name="firstPriority"
					onchange="setConstraints(1)">
						<?php echo $job_options; ?>
					</select>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">جنسیت:</p>
				<p id="pSex_1"></p>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">مدرک مورد نیاز:</p>
				<p id="pCertificate_1"></p>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">سابقه کاری:</p>
				<p id="pExperience_1"></p>
			</div>
		</div>
		<br> <br> <br>
		<legend>اولویت دوم</legend>
		<div class="row">
			<div class="col-md-12">
				<label for="secondPriority">شغل:</label> <select id="priority_2"
					name="secondPriority" onchange="setConstraints(2)">
					<option value="">انتخاب کنید...</option>
						<?php echo $job_options; ?>
					</select>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">جنسیت:</p>
				<p id="pSex_2"></p>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">مدرک مورد نیاز:</p>
				<p id="pCertificate_2"></p>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">سابقه کاری:</p>
				<p id="pExperience_2"></p>
			</div>
		</div>
		<br> <br> <br>
		<legend>اولویت سوم</legend>
		<div class="row">
			<div class="col-md-12">
				<label for="thirdPriority">شغل:</label> <select id="priority_3"
					name="thirdPriority" onchange="setConstraints(3)">
					<option value="">انتخاب کنید...</option>
						<?php echo $job_options; ?>
					</select>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">جنسیت:</p>
				<p id="pSex_3"></p>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">مدرک مورد نیاز:</p>
				<p id="pCertificate_3"></p>
			</div>
			<div class="col-md-4 col-sm-6">
				<p class="pale">سابقه کاری:</p>
				<p id="pExperience_3"></p>
			</div>
		</div>

	</fieldset>




	<!-- گام پنجم  -->
	<h3>تعهدات</h3>
	<fieldset>
		<legend>تعهدات مربوطه</legend>
		<div class="row">
			<div class="col-xs-12">
				<p>اینجانب</p>
				<p id="subscriberName"></p>
				<p>کليه مندرجات قيد شده در اين پرسشنامه را بطور صحيح تکميل نمودم و
					چنانچه مواردي خلاف واقع مشاهده گردد، شرکت مجاز است همکاري خود را با
					اينجانب راسـا و يکجانبه قطع و بنا به ميل خود در آن تجديد نظر نمايد.</p>
			</div>
			<div class="col-md-4 col-sm-6">
				<br> <input type="submit" class="btn btn-default"
					value="قبول و ارسال فرم" />
			</div>
		</div>
	</fieldset>
</form>

<?php include_layout_template ( 'footer.php' );?>
