<?php
require_once("../includes/initialize.php");
include_layout_template('header.php');

if (isset($_POST[firstPriority])) {
    $firstPriority = filter_input(INPUT_POST, 'firstPriority', FILTER_SANITIZE_NUMBER_INT);
    $secondPriority = filter_input(INPUT_POST, 'secondPriority', FILTER_SANITIZE_NUMBER_INT);
    $thirdPriority = filter_input(INPUT_POST, 'thirdPriority', FILTER_SANITIZE_NUMBER_INT);
    $national_number = filter_input(INPUT_POST, 'nationalNumber', FILTER_SANITIZE_NUMBER_INT);
    $record_id = filter_input(INPUT_POST, 'recordID', FILTER_SANITIZE_NUMBER_INT);
    $query = sprintf("UPDATE Records SET firstPriority=$firstPriority, "
            . "secondPriority=$secondPriority, thirdPriority=$thirdPriority, "
            . "updated=1 "
            . "WHERE recordID=$record_id AND nationalNumber=$national_number");
    $result_set = $database->query($query);
    $message = ($result_set) ? "اولویت‌های شغلی شما با موفقیت ویرایش شد."
            : "مشکلی رخ داد، دوباره امتحان کنید.";
    redirect_to($_SERVER['PHP_SELF'] . "?message=$message");
}
if (isset($_POST[nationalNumber]) || isset($_POST[trackingNumber])) {
    $national_number = filter_input(INPUT_POST, 'nationalNumber', FILTER_SANITIZE_NUMBER_INT);
    $tracking_number = filter_input(INPUT_POST, 'trackingNumber', FILTER_SANITIZE_NUMBER_INT);
    if (substr($tracking_number, 0, 3) == "386") {
        $record_id = substr($tracking_number, 3);
        $query = sprintf("SELECT firstPriority, secondPriority, thirdPriority, "
                . "fullName, updated FROM Records "
                . "WHERE recordID=$record_id AND nationalNumber=$national_number");
        $result_set = $database->query($query);
    }
    if ($result_set) {
        $record = mysql_fetch_assoc($result_set);
        if ($record['updated'] == "0") {
            // Query job data from database
            $query = sprintf("SELECT JobID, JobName, Sex, RequiredCertificate,
		Experience FROM `Jobs` WHERE Open=1 group by JobName order by JobID");
            $result_set = $database->query($query);

            $rows = array();
            while ($row = mysql_fetch_assoc($result_set))
                $rows [] = $row;

            $job_options = "<option value=''>انتخاب کنید...</option>";
            $job_options .= "<option value='' style='color:#999' >"
                    . "------------- مشاغل بخش فود کورت -------------</option>";
            $temp_id = "";
            foreach ($rows as $key => $row) {
                $job_id = $row ['JobID'];
                $job_name = $row ['JobName'];
                if (intval($temp_id) <= 178 && intval($job_id) >= 180)
                    $job_options .= "<option value='' style='color:#999' >------------- مشاغل بخش هایپرمارکت -------------</option>";
                $job_options .= '<option value="' . $job_id . '">' . $job_name . '</option>';
                $temp_id = $job_id;
            }
            ?>
            <script>
                var jobs = <?php echo json_encode($rows); ?>;
                var record = <?php echo json_encode($record); ?>;
                var applicatedJobs = <?php echo json_encode(getApplicatedJobs()); ?>;
            </script>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">

                <h2><?php echo $record['fullName']; ?></h2>
                <fieldset>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul>
                                <li>در صورت ویرایش اولویت‌های شغلی دیگر قادر به ویرایش آنها 
                                    <span class="error">نخواهید بود</span>.</li>
                                <li>ثبت نام و انتخاب مشاغل در این سامانه هیچ حقی را برای متقاضی و
                                    هیچ تعهدی را مبنی بر استخدام برای شرکت ایجاد نمی کند و هر داوطلب
                                    باید با توجه به شرایط تخصصی و علمی خود و پس از مطالعه و تایید
                                    شرایط اختصاصی، شغل را به عنوان اولویت انتخابی خود ثبت نماید.</li>
                                <li>هر متقاضی می‌تواند از بین مشاغل زیر بر حسب توانایی‌های خود ۳
                                    شغل را به ترتیب اولویت انتخاب کند.</li>
                                <li>بدیهی است چنانچه متقاضی هریک از شرایط اختصاصی را نداشته باشد یا
                                    نتواند مدارک خواسته شده را ارائه نماید از مراحل گزینش حذف خواهد
                                    شد.</li>
                            </ul>
                        </div>

                        <br>
                    </div>
                    <legend>اولویت اول</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="firstPriority">شغل:<span style="color: red">*</span></label>
                            <select id="priority_1" required name="firstPriority"
                                    onchange="setConstraints(1)">
                                        <?php echo $job_options; ?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">جنسیت:</span>
                            <span id="pSex_1"></span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">مدرک مورد نیاز:</span>
                            <span id="pCertificate_1"></span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">سابقه کاری:</span>
                            <span id="pExperience_1"></span>
                        </div>
			<div class="col-md-4 col-sm-6">
				<span class="pale">تعداد متقاضیان:</span>
				<span id="pApplicant_1"></span>
			</div>
                    </div>
                    <br> <br> <br>
                    <legend>اولویت دوم</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="secondPriority">شغل:</label> <select id="priority_2"
                                name="secondPriority" onchange="setConstraints(2)">
                                <option value="">انتخاب کنید...</option>
                                <?php echo $job_options; ?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">جنسیت:</span>
                            <span id="pSex_2"></span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">مدرک مورد نیاز:</span>
                            <span id="pCertificate_2"></span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">سابقه کاری:</span>
                            <span id="pExperience_2"></span>
                        </div>
			<div class="col-md-4 col-sm-6">
				<span class="pale">تعداد متقاضیان:</span>
				<span id="pApplicant_2"></span>
			</div>
                    </div>
                    <br> <br> <br>
                    <legend>اولویت سوم</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="thirdPriority">شغل:</label> <select id="priority_3"
                                name="thirdPriority" onchange="setConstraints(3)">
                                <option value="">انتخاب کنید...</option>
                                <?php echo $job_options; ?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">جنسیت:</span>
                            <span id="pSex_3"></span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">مدرک مورد نیاز:</span>
                            <span id="pCertificate_3"></span>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <span class="pale">سابقه کاری:</span>
                            <span id="pExperience_3"></span>
                        </div>
			<div class="col-md-4 col-sm-6">
				<span class="pale">تعداد متقاضیان:</span>
				<span id="pApplicant_3"></span>
			</div>
                    </div>

                </fieldset>
                <input type="hidden" name="recordID" value="<?php echo $record_id ?>" />
                <input type="hidden" name="nationalNumber" value="<?php echo $national_number ?>" />
                <br>
                <input class="btn pull-left" type="submit" value="ارسال" />
                <br>
            </form>



            <?php
        } else {
            redirect_to($_SERVER['PHP_SELF']
                . "?message=اولویت‌های شغلی حداکثر یک بار قابل ویرایش است.");
        }
    } else {
        redirect_to($_SERVER['PHP_SELF'] . "?message=سابقه‌ای پیدا نشد.");
    }
}
if (!isset($record)) {
    ?>
    <div class="row">
        <div class="col-sm-3" id="login-div">
            <p class="bg-warning"><?php echo $_GET['message'] ?></p>
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                <label for="trackingNumber">کد رهگیری:</label>
                <input name="trackingNumber" id="trackingNumber" type="number"
                       class="login-elements" required pattern="[0-9]{5,7}"
                       minlength="5" maxlength="7"/><br>
                <label for="nationalNumber">کد ملی:</label>
                <input name="nationalNumber" id="nationalNumber" type="number"
                       class="login-elements" required pattern="[0-9]{10}"
                       minlength="10" maxlength="10" /><br>
                <input class="btn login-elements" type="submit" value="ارسال">
            </form>
        </div>
    </div>
    <?php
}
include_layout_template('footer.php');
?>
<script>
    $("#priority_1").val(record.firstPriority);
    $("#priority_2").val(record.secondPriority);
    $("#priority_3").val(record.thirdPriority);
    setConstraints(1);
    setConstraints(2);
    setConstraints(3);
</script>
