<!DOCTYPE HTML>
<html dir="rtl" lang="fa-IR">
<head>

<!-- metas -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="geo.region" content="IR" />
<meta name="geo.placename" content="Kashan" />
<meta name="geo.position" content="33.989625, 51.440173" />
<meta name="ICBM" content="33.989625, 51.440173" />

<!-- stylesheets -->
<link href="stylesheets/main.css" media="all" rel="stylesheet"
	type="text/css" />
<link href="stylesheets/jquery.steps.css" media="all" rel="stylesheet"
	type="text/css" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- RTL bootstrap plugin -->
<link rel="stylesheet" href="stylesheets/bootstrap-rtl.min.css">
<!-- Persian date picker -->
<link rel="stylesheet" href="stylesheets/persian-datepicker-0.4.5.min.css">

<!-- scripts -->


<title>فرم درخواست ثبت نام</title>
</head>
<body>
	<div id="header">
	</div>
	<div class="container">
		<div id="main">