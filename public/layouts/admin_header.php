<!DOCTYPE HTML>
<html dir="rtl" lang="fa-IR">
<head>
<title>مدیریت</title>
<!-- metas -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, 
      maximum-scale=1, user-scalable=no">

<!-- stylesheets -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../stylesheets/bootstrap-rtl.min.css">
<link href="../stylesheets/main.css" media="all" rel="stylesheet" type="text/css" />
<link href="../stylesheets/admin.css" media="all" rel="stylesheet" type="text/css" />

<!-- scripts -->
<script src="../javascripts/jquery-3.1.0.min.js"></script>
</head>
<body>
	<div id="header">
		<h2>مدیریت سامانه ثبت‌نام متقاضیان استخدام</h2>
	</div>
	<div class="container">
		<div id="main">
