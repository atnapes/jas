<?php
require_once('../../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
include_layout_template('admin_header.php');

$from = (isset($_GET['from'])) ?
        filter_input(INPUT_GET, 'from', FILTER_SANITIZE_NUMBER_INT) : 41;
$to = (isset($_GET['to'])) ?
        filter_input(INPUT_GET, 'to', FILTER_SANITIZE_NUMBER_INT) : 200;

// Query job data from database
$query = sprintf("SELECT recordID, fullName, fatherName,
                nationalNumber, birthDate, gender, maritalStatus
		FROM `Records` WHERE recordID BETWEEN $from AND $to");
$result_set = mysql_query($query);
if (!$result_set) {
    $message = 'Invalid query: ' . mysql_error() . "\n";
    $message .= 'Whole query: ' . $query;
    log_action('error', $message);
    die();
}
$rows = array();
while ($row = mysql_fetch_assoc($result_set))
    $rows [] = $row;
?>

<div class="row toolbar">
    <div class="col-md-6">
        <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            از: <input name="from" id="from" type="number" min="0" max="99999"
                       value="<?php echo $from; ?>"/>
            تا: <input name="to" id="to" type="number" min="1" max="99999"
                       value="<?php echo $to; ?>"/>
            <input type="submit" value="نشان دادن">
        </form>
    </div>
    <div class="col-md-6">
        <ul id="menu" class="pull-left">
            <li><a href="logout.php">خارج شدن</a></li>
            <li><a href="export.php">برون‌ریزی</a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <table id="recordsTable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>نام و نام خانوادگی</th>
                    <th>نام پدر</th>
                    <th>کد ملی</th>
                    <th>تاریخ تولد</th>
                    <th>جنسیت</th>
                    <th>وضعیت تاهل</th>
                </tr>
            </thead>
            <tfoor></tfoor>
            <tbody>
<?php foreach ($rows as $key => $row): ?>
                    <tr>
                        <td><a href="profile.php?id=<?php echo $row['recordID'] ?>"></a>
    <?php echo $row['recordID'] ?></td>
                        <td><?php echo $row['fullName'] ?></td>
                        <td><?php echo $row['fatherName'] ?></td>
                        <td><?php echo $row['nationalNumber'] ?></td>
                        <td><?php echo $row['birthDate'] ?></td>
                        <td><?php echo $row['gender'] ?></td>
                        <td><?php echo $row['maritalStatus'] ?></td>
                    </tr>
<?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<?php include_layout_template('admin_footer.php'); ?>
		
