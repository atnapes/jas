<?php require_once("../../includes/initialize.php"); ?>
<?php

log_action ( 'Log out', "user id {$_SESSION['user_id']} logged out." );
$session->logout ();
redirect_to ( "login.php" );
?>
