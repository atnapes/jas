<?php
require_once ('../../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
include_layout_template('admin_header.php');

define(RECORDS, __DIR__. '/export/records.csv');

if ($_GET ['action'] == "renew") {
    // fetching all the records
    $query = "select * from Records";
    $result_set = $database->query($query);
    $records = array();
    while ($row = mysql_fetch_assoc($result_set))
        $records [] = $row;

    export_csv ($records, RECORDS);
    $message = "پرونده برون‌ریزی تازه‌سازی شد.";
}
if ($_GET ['action'] == "download-export-file") {
    if (file_exists(RECORDS)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename(RECORDS));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize(RECORDS));
        ob_clean();
        flush();
        readfile(RECORDS);
        exit;
    }
}
?>
<div class="row toolbar">
    <div class="col-md-6">
        <a href="<?php $_SERVER['PHP_SELF'] ?>?action=renew"><h4>تازه‌سازی پرونده برون‌ریزی</h4></a>
    </div>
    <div class="col-md-6">
        <ul id="menu" class="pull-left">
            <li><a href="index.php">صفحه اصلی</a></li>
            <li><a href="jobs_control.php">مدیریت مشاغل</a></li>
            <li><a href="logout.php">خارج شدن</a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">

    </div>
    <div class="col-xs-12">
        <p><?php echo $message ?></p>
        <p>برون‌ریزی شده در تاریخ‌: <?php
            include_once '../../includes/jdf.php';
            echo jstrftime('%e %B %Y %H:%M:%S', filemtime(RECORDS))
            ?></p>
        <a href="<?php $_SERVER['PHP_SELF'] ?>?action=download-export-file" target="_blank">دانلود پرونده برون‌ریزی</a>
    </div>


</div>

<?php include_layout_template('admin_footer.php'); ?>