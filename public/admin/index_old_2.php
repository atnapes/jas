<?php
require_once('../../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
include_layout_template('admin_header.php');

define(DOWNLOAD_TEMP, __DIR__. '/export/custom_query.csv');

$job = filter_input(INPUT_GET, 'job', FILTER_SANITIZE_NUMBER_INT);
$sort_1 = filter_input(INPUT_GET, 'sort_1', FILTER_SANITIZE_STRING);
$sort_2 = filter_input(INPUT_GET, 'sort_2', FILTER_SANITIZE_STRING);
$quantity = filter_input(INPUT_GET, 'quantity', FILTER_SANITIZE_NUMBER_INT);
$quantity = ($quantity) ? $quantity : 200;

// Query job data from database
$query = sprintf("SELECT * FROM `Records` ");
if ($job != ""){
    $query .= " WHERE firstPriority='$job'";
    $query .= " OR secondPriority='$job' OR thirdPriority='$job'";
}
if ($sort_1 != "") {
    $query .= " ORDER BY $sort_1";
    if ($sort_2 != "")
        $query .= ", $sort_2";
}
$query .= " LIMIT 0, $quantity";
$result_set = mysql_query($query);
$rows = array();
while ($row = mysql_fetch_assoc($result_set))
    $rows [] = $row;
if ($_GET['action'] == "download") {
    export_csv($rows, DOWNLOAD_TEMP);
    if (file_exists(DOWNLOAD_TEMP)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename(DOWNLOAD_TEMP));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize(DOWNLOAD_TEMP));
        ob_clean();
        flush();
        readfile(DOWNLOAD_TEMP);
        exit;
    }
}

// fetching all the jobs
$query = sprintf("SELECT JobID, JobName FROM Jobs");
$result_set = $database->query($query);
$jobs = array();
while ($row = mysql_fetch_assoc($result_set))
    $jobs [$row ['JobID']] = $row ['JobName'];
$job_options = "<option value=''>همه مشاغل</option>";
foreach ($jobs as $job_id => $job_name) {
    $job_options .= '<option value="' . $job_id . '">' . $job_name . '</option>';
}
?>
<div class="row toolbar">
    <div class="col-sm-8">
        <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <table class="table">
                <tbody>
                    <tr>
                        <td><label for="job">شغل</label></td>
                        <td>
                            <select name="job" id="job">
                                <?php echo $job_options ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="sort_1">ترتیب:</label></td>
                        <td>
                            <select name="sort_1" id="sort_1">
                                <option value="">-</option>
                                <option value="gender" <?php if ($sort_1 == 'gender') echo "selected"; ?>>جنسیت</option>
                                <option value="maritalStatus" <?php if ($sort_1 == 'maritalStatus') echo "selected"; ?>>وضعیت تاهل</option>
                                <option value="conscriptionStatus" <?php if ($sort_1 == 'conscriptionStatus') echo "selected"; ?>>وضعیت نظام وظیفه</option>
                                <option value="firstPriority" <?php if ($sort_1 == 'firstPriority') echo "selected"; ?>>اولویت اول</option>
                                <option value="secondPriority" <?php if ($sort_1 == 'secondPriority') echo "selected"; ?>>اولویت دوم</option>
                                <option value="thirdPriority" <?php if ($sort_1 == 'thirdPriority') echo "selected"; ?>>اولویت سوم</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="sort_2">ترتیب (۲):</label></td>
                        <td>
                            <select name="sort_2" id="sort_2">
                                <option value="">-</option>
                                <option value="gender" <?php if ($sort_2 == 'gender') echo "selected"; ?>>جنسیت</option>
                                <option value="maritalStatus" <?php if ($sort_2 == 'maritalStatus') echo "selected"; ?>>وضعیت تاهل</option>
                                <option value="conscriptionStatus" <?php if ($sort_2 == 'conscriptionStatus') echo "selected"; ?>>وضعیت نظام وظیفه</option>
                                <option value="firstPriority" <?php if ($sort_2 == 'firstPriority') echo "selected"; ?>>اولویت اول</option>
                                <option value="secondPriority" <?php if ($sort_2 == 'secondPriority') echo "selected"; ?>>اولویت دوم</option>
                                <option value="thirdPriority" <?php if ($sort_2 == 'thirdPriority') echo "selected"; ?>>اولویت سوم</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="quantity">تعداد</label></td>
                        <td>
                            <input name="quantity" id="from" type="number" min="0"
                                   max="99999" value="<?php echo $quantity; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" value="نشان دادن">
                            <a class="btn" href="<?php echo $_SERVER['PHP_SELF'] . "?". $_SERVER['QUERY_STRING']?>&action=download">دانلود</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="col-sm-4">
        <ul id="menu" class="pull-left">
            <li><a href="logout.php">خارج شدن</a></li>
            <li><a href="export.php">برون‌ریزی</a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <table id="recordsTable" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>نام و نام خانوادگی</th>
                    <th>کد ملی</th>
                    <th>تاریخ تولد</th>
                    <th>جنسیت</th>
                    <th>و تاهل</th>
                    <th>و وظیفه</th>
                    <th>ت فرزندان</th>
                    <th>اولویت اول</th>
                    <th>اولویت دوم</th>
                    <th>اولویت سوم</th>
                    <th>آدرس</th>
                </tr>
            </thead>
            <tfoor></tfoor>
            <tbody>
                <?php foreach ($rows as $key => $row): ?>
                    <tr>
                        <td><a href="profile.php?id=<?php echo $row['recordID'] ?>"></a>
                            <?php echo $row['recordID'] ?></td>
                        <td><?php echo $row['fullName'] ?></td>
                        <td><?php echo $row['nationalNumber'] ?></td>
                        <td><?php echo $row['birthDate'] ?></td>
                        <td><?php echo $row['gender'] ?></td>
                        <td><?php echo $row['maritalStatus'] ?></td>
                        <td><?php echo $row['conscriptionStatus'] ?></td>
                        <td><?php echo $row['childrenNumber'] ?></td>
                        <td><?php echo $jobs[$row['firstPriority']] ?></td>
                        <td><?php echo $jobs[$row['secondPriority']] ?></td>
                        <td><?php echo $jobs[$row['thirdPriority']] ?></td>
                        <td id="addressTd"><?php echo $row['address'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    var fp = <?php echo $job; ?>;
    $("#job").val(fp);
</script>

<?php include_layout_template('admin_footer.php'); ?>
		
