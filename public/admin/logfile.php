<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) {
    redirect_to("login.php");
} ?>
<?php
$logfile = SITE_ROOT . DS . 'logs' . DS . 'log.txt';

if ($_GET['clear'] == 'true') {
    file_put_contents($logfile, '');
    // Add the first log entry
    log_action('Logs Cleared', "by User ID {$session->user_id}");
    // redirect to this same page so that the URL won't 
    // have "clear=true" anymore
    redirect_to('logfile.php');
}
?>

<?php include_layout_template('admin_header.php'); ?>

<div class="row toolbar">
    <div class="col-md-6">
        <h2>پرونده لاگ</h2>
    </div>
    <div class="col-md-6">
        <ul id="menu" class="pull-left">
            <li><a href="index.php">صفحه اصلی</a></li>
            <li><a href="jobs_control.php">مدیریت مشاغل</a></li>
            <li><a href="logout.php">خارج شدن</a></li>
            <li><a href="logfile.php?clear=true">Clear log file</a></li>
        </ul>
    </div>
</div>

<br />



<!-- <p><p> -->

<?php
if (file_exists($logfile) && is_readable($logfile) &&
        $handle = fopen($logfile, 'r')) {  // read
    echo "<ul class=\"log-entries\">";
    $offset = -100000;
    fseek($handle, $offset, SEEK_END);
    while (!feof($handle)) {
        $entry = fgets($handle);
        if (trim($entry) != "") {
            echo "<li>{$entry}</li>";
        }
    }
    echo "</ul>";
    fclose($handle);
} else {
    echo "Could not read from {$logfile}.";
}
?>
h<
?php include_layout_template('admin_footer.php'); ?>
