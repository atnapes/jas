<?php
require_once('../../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
include_layout_template('admin_header.php');

if (isset($_GET['toggleStatus'])) {
    $toggleJobID = filter_input(INPUT_GET, 'toggleJobID', FILTER_SANITIZE_NUMBER_INT);
    $toggleStatus = filter_input(INPUT_GET, 'toggleStatus', FILTER_SANITIZE_NUMBER_INT);
    $toggleStatus = ($toggleStatus == "1") ? 0 : 1; 
    $query = sprintf("UPDATE Jobs SET Open=$toggleStatus WHERE JobID=$toggleJobID");
    $result_set = $database->query($query);
}

// fetching all the jobs
$query = sprintf("SELECT JobID, JobName, Quantity, Sex, RequiredCertificate, "
        . "Experience, CAST(Open AS UNSIGNED) AS Open FROM `Jobs` ORDER BY JobID");
$result_set = $database->query($query);
$jobs = array();
while ($row = mysql_fetch_assoc($result_set)){
    $jobs [] = $row;
}
$selected_jobs = getApplicatedJobs();
?>
<div class="row toolbar">
    <div class="col-sm-4 pull-left">
        <ul id="menu" class="pull-left">
            <li><a href="index.php">صفحه اصلی</a></li>
            <li><a href="logout.php">خارج شدن</a></li>
            <li><a href="export.php">برون‌ریزی</a></li>
        </ul>
    </div>
    <div class="col-sm-8">

    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
                <tr>
                    <th>شغل</th>
                    <th>جنسیت</th>
                    <th>مدرک</th>
                    <th>سابقه</th>
                    <th>ظرفیت</th>
                    <th>تعداد</th>
                    <th>وضعیت</th>
                </tr>
            </thead>
            <tbody>
<?php foreach ($jobs as $job): ?>
                    <tr>
                        <td><?php echo $job[JobName] ?></td>
                        <td><?php echo ($job[Sex]) ? $job[Sex] : "-" ?></td>
                        <td><?php echo substr($job[RequiredCertificate], 0, 35) ?></td>
                        <td><?php echo ($job[Experience]) ? $job[Experience] . " سال" : "-"?></td>
                        <td><?php echo $job[Quantity] ?></td>
                        <td><?php echo ($selected_jobs[$job[JobID]]) ? $selected_jobs[$job[JobID]] : 0 ?></td>
                        <td><a href="?toggleStatus=<?php echo $job[Open]. "&toggleJobID=" . $job[JobID]?>"/>
                            <?php echo ($job[Open] == "1") ? "باز" : "بسته" ?></td>
                    </tr>
<?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php include_layout_template('admin_footer.php'); ?>