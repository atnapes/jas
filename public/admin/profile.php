<?php
require_once ('../../includes/initialize.php');
if (! $session->is_logged_in ()) {
	redirect_to ( "login.php" );
}
include_layout_template ( 'admin_header.php' );

$id = $_GET ['id'];
$action = ($_GET['action'] == "prev") ? -1 : 1;

// Query job data from database
$query = sprintf ( "SELECT * FROM Records WHERE recordID=$id" );
$result_set = $database->query ( $query );
$record = mysql_fetch_assoc ( $result_set );
$newID = $id + $action;



$query = sprintf ( "SELECT JobID, JobName FROM Jobs" );
$job_results = $database->query ( $query );

$jobs = array ();
while ( $row = mysql_fetch_assoc ( $job_results ) )
	$jobs[$row['JobID']] = $row['JobName'];

$studies = unserialize($record['studies']);
$works = unserialize($record['works']);
$computerCourses = unserialize($record['computerCourses']);
$computerProficiencies = unserialize($record['computerProficiencies']);
$languages = unserialize($record['languages']);
$certificates = unserialize($record['certificates']);


?>

<div class="row toolbar">
	<div class="col-md-6">
		<a href="profile.php?id=<?php echo $id -1?>&action=prev">&lt; قبلی</a> | 
		<a href="profile.php?id=<?php echo $id +1?>">بعدی &gt;</a>
	</div>
	<div class="col-md-6">
		<ul id="menu" class="pull-left">
			<li><a href="index.php">صفحه اصلی</a></li>
                        <li><a href="jobs_control.php">مدیریت مشاغل</a></li>
			<li><a href="logout.php">خارج شدن</a></li>
		</ul>
	</div>
</div>
<?php 
if (!$record) {
	if ($_GET['action'] == "redir") die('سابقه‌ای وجود ندارد');
	redirect_to($_SERVER['PHP_SELF'] . "?id=" . $newID . "&action=redir");
}
?>

<div class="row">
	<div class="col-sm-4 pull-left">
		<img class="img-thumbnail" alt="profile_picture"
			src="../_assets/<?php echo $record['recordID']; ?>.jpg">
	</div>
	<div class="col-sm-8">
		<h2><?php echo $record['fullName']?></h2><br>
		<table class="table table-striped">
			<tr>
				<td>کد پیگیری</td>
				<td><?php echo "386" . $record['recordID']?></td>
			</tr>
			<tr>
				<td>نام مستعار</td>
				<td><?php echo $record['alias']?></td>
			</tr>
			<tr>
				<td>نام پدر</td>
				<td><?php echo $record['fatherName']?></td>
			</tr>
			<tr>
				<td>شماره ملی</td>
				<td><?php echo $record['nationalNumber']?></td>
			</tr>
			<tr>
				<td>محل تولد</td>
				<td><?php echo $record['birthPlace']?></td>
			</tr>
			<tr>
				<td>تاریخ تولد</td>
				<td><?php echo $record['birthDate']?></td>
			</tr>
			<tr>
				<td>ضعیت تاهل</td>
				<td><?php echo $record['maritalStatus']?></td>
			</tr>
			<tr>
				<td>جنسیت</td>
				<td><?php echo $record['gender']?></td>
			</tr>
			<tr>
				<td>وضعیت نظام وظیفه</td>
				<td><?php echo $record['conscriptionStatus']?></td>
			</tr>
			<tr>
				<td>تعداد فرزندان</td>
				<td><?php echo $record['childrenNumber']?></td>
			</tr>
			<tr>
				<td>تلفن</td>
				<td><?php echo $record['phoneNumber']?></td>
			</tr>
			<tr>
				<td>تلفن همراه</td>
				<td><?php echo $record['cellPhoneNumber']?></td>
			</tr>
			<tr>
				<td>ایمیل</td>
				<td><?php echo $record['email']?></td>
			</tr>
			<tr>
				<td>نشانی</td>
				<td><?php echo $record['address']?></td>
			</tr>
			<tr>
				<td>تلفن محل کار فعلی</td>
				<td><?php echo $record['currentWorkplacePhone']?></td>
			</tr>
			<tr>
				<td>نشانی محل کار فعلی</td>
				<td><?php echo $record['currentWorkplaceAddress']?></td>
			</tr>
			<tr>
				<td>تمایل به همکاری بصورت</td>
				<td><?php echo $record['intendedWorkType']?></td>
			</tr>
			<tr>
				<td>حقوق مورد نظر</td>
				<td><?php echo $record['intendedSalary']?></td>
			</tr>
			<tr>
				<td>گواهینامه رانندگی</td>
				<td><?php foreach(unserialize($record['driverLicense']) as $value) echo $value . " - ";?></td>
			</tr>
			<tr>
				<td>اولویت اول</td>
				<td><?php echo $jobs[$record['firstPriority']] ?></td>
			</tr>
			<tr>
				<td>اولویت دوم</td>
				<td><?php echo $jobs[$record['secondPriority']] ?></td>
			</tr>
			<tr>
				<td>اولویت سوم</td>
				<td><?php echo $jobs[$record['thirdPriority']] ?></td>
			</tr>
		</table>

	</div>
</div>
<?php if (!empty_subarrays($studies)):?>
<div class="row">
	<h3>سوابق تحصیلی</h3>
	<div class="col-xs-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>مقطع</th>
					<th>رشته تحصیلی</th>
					<th>گرایش</th>
					<th>مدت تحصیل از تاریخ</th>
					<th>تا تاریخ</th>
					<th>نام واحد آموزشی</th>
					<th>کشور/شهر محل تحصیل</th>
					<th>معدل</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $studies as $record ) {
				if (!empty_array($record)) {
					echo "<tr>";
					foreach ( $record as $value )
						echo "<td>$value</td>";
					echo "</tr>";
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php endif;?>
<?php if (!empty_subarrays($works)):?>
<div class="row">
	<h3>سوابق حرفه‌ای</h3>
	<div class="col-xs-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>نام سازمان</th>
					<th>مدت همکاری از تاریخ</th>
					<th>تا تاریخ</th>
					<th>سمت</th>
					<th>حقوق مزایا</th>
					<th>علت قطع رابطه</th>
					<th>گواهی سابقه کار</th>
					<th>نوع کار</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $works as $record ) {
				if (!empty_array($record)) {
					echo "<tr>";
					foreach ( $record as $value )
						echo "<td>$value</td>";
					echo "</tr>";
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php endif;?>
<?php if (!empty_subarrays($computerCourses)):?>
<div class="row">
	<h3>دوره‌های آموزش کامپیوتر</h3>
	<div class="col-xs-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>نام دوره</th>
					<th>سطح</th>
					<th>محل آموزش</th>
					<th>سال</th>
					<th>میزان ساعات دوره</th>
					<th>گواهی پایان دوره</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $computerCourses as $record ) {
				if (!empty_array($record)) {
					echo "<tr>";
					foreach ( $record as $value )
						echo "<td>$value</td>";
					echo "</tr>";
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php endif;?>
<?php if (!empty_subarrays($computerProficiencies)):?>
<div class="row">
	<h3>مهارت کامپیوتری</h3>
	<div class="col-md-7">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>نام برنامه</th>
					<th>میزان تسلط</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $computerProficiencies as $record ) {
				if (!empty_array($record)) {
					echo "<tr>";
					foreach ( $record as $value )
						echo "<td>$value</td>";
					echo "</tr>";
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php endif;?>
<?php if (!empty_subarrays($languages)):?>
<div class="row">
	<h3>مهارت در زبان خارجی</h3>
	<div class="col-md-7">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>زبان خارجی</th>
					<th>نوع توانایی</th>
					<th>میزان تسلط</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $languages as $record ) {
				if (!empty_array($record)) {
					?>
					<tr>
						<td><?php echo $record['language']?></td>
						<td><?php foreach($record['proficiencyType'] as $value) echo $value . " - ";?></td>
						<td><?php echo $record['proficiency']?></td>
					</tr>
					<?php
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php endif;?>
<?php if (!empty_subarrays($certificates)):?>
<div class="row">
	<h3>گواهینامه‌های علمی تخصصی اخذ شده</h3>
	<div class="col-xs-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>نوع مدرک</th>
					<th>محل اخذ</th>
					<th>سال اخذ</th>
					<th>میزان ساعات دوره</th>
					<th>گواهی پایان دوره</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $certificates as $record ) {
				if (!empty_array($record)) {
					echo "<tr>";
					foreach ( $record as $value )
						echo "<td>$value</td>";
					echo "</tr>";
				}
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php endif;?>


<?php include_layout_template('admin_footer.php'); ?>
		
