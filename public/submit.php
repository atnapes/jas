<?php require_once("../includes/initialize.php"); ?>

<?php include_layout_template('header.php'); ?>

<?php

$success = $_GET['success'];
$tracking_code = en_to_fa($_GET['tracking_code']);

log_action("submit.php", "success = $success, tracking_code = $tracking_code");
?>

<div class="row">
	<div class="col-md-12 text-center">
		<br><br>
		<?php if ($_GET['success'] == 1): ?>
		<h4><?php echo  "اطلاعات شما با کد پیگیری <strong>$tracking_code</strong> ثبت شد در صورت تائيد اوليه و قرارگرفتن در ليست افرادى كه شرايط مورد نظر را دارند ،جهت مصاحبه حضورى با شما تماس گرفته خواهد شد."; ?></h4>
		<?php log_action('success message showed')?>
		<a href="message.php" role="button" class="btn btn-default">ثبت‌نام متقاضی دیگر</a>
		<?php else :?>
		<h4>متاسفانه سامانه در ثبت اطلاعات دچار مشکل شد.</h4>
		<a href="form.php" role="button" class="btn btn-default">دوباره امتحان کنید</a>
		<?php log_action('submit failure')?>
		<?php endif;?>
	</div>
</div>
<?php include_layout_template ( 'footer.php' );?>